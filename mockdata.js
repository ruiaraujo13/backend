'use strict';

const fs = require('fs');
const faker = require('faker');

const images = [
	'abstract',
	'animals',
	'business',
	'cats',
	'city',
	'food',
	'nightlife',
	'fashion',
	'people',
	'nature',
	'sports',
	'technics',
	'transport'
];
const news = [];

for (let i = 0; i < 100; i++) {
	const title = faker.random.words();

	news.push({
		id: faker.random.uuid(),
		title: title.charAt(0).toUpperCase() + title.slice(1),
		body: faker.lorem.paragraphs(),
		image: faker.image[`${images[Math.floor(Math.random() * images.length)]}`](),
		date: faker.date.past(),
		author: faker.name.findName()

	});
}

fs.writeFileSync('./db/news.json', JSON.stringify({
	news
}));

console.log('Data Mocked!');
