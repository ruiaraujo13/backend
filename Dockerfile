FROM node:11.9.0-alpine

RUN mkdir -p /usr/scr/app

COPY . /usr/src/app
WORKDIR /usr/src/app
RUN rm /usr/src/app/Dockerfile && \
	rm /usr/src/app/README.md && \
	npm install --production

ENV PORT 80

EXPOSE 80

CMD [ "npm", "start" ]
