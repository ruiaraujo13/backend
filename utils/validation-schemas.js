'use strict';

const joi = require('joi');

module.exports = {
	v1: {
		news: {
			getAll: {
				query: joi.object().keys({
					orderBy: joi.string().valid(['date', 'author', 'title']),
					orderType: joi.string().valid(['asc', 'desc']),
					filter: joi.string(),
					page: joi.number().integer().min(1),
					perPage: joi.number().integer().min(1)
				}).unknown(false)
			},

			getOne: {
				params: joi.object().keys({
					id: joi.string().required().uuid({
						version: ['uuidv4']
					}).lowercase()
				}).unknown(false)
			}
		}
	}
};
