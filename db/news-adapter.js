'use strict';

const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

module.exports = class News {
	constructor() {
		this._DB = low(new FileSync(path.join(__dirname, './news.json')));
	}

	async findAll({
		orderBy = 'date',
		orderType = 'desc',
		filter = null,
		page = 1,
		perPage = 10
	}) {
		const data = filter ? this._DB.get('news').filter(news => (news.title.toLowerCase().includes(filter.toLowerCase()))) : this._DB.get('news');

		const total = data.size();
		const from = (page - 1) * perPage;
		const to = from + perPage;

		return {
			total,
			perPage,
			currentPage: page,
			lastPage: Math.ceil(total / perPage),
			data: data.orderBy(orderBy, orderType).slice(from, to).map(news => ({
				id: news.id,
				title: news.title,
				body: news.body.substring(0, 75),
				image: news.image,
				date: news.date,
				author: news.author
			}))
		};
	}

	async findOneById({
		id
	}) {
		const news = await this._DB.get('news').find({
			id
		}).value();

		if (news) {
			return news;
		}

		throw new Error('not found');
	}
};
