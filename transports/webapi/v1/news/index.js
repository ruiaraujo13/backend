'use strict';

const express = require('express');
const wrapAsync = require('express-async-wrapper');
const requestValidator = require('express-validation');

const Schemas = require('../../../../utils/validation-schemas').v1.news;
const {
	News
} = require('../../../../core');

const router = express.Router(); // eslint-disable-line new-cap

module.exports = () => {
	const NEWS = new News();

	router.get('/', requestValidator(Schemas.getAll), wrapAsync(async (req, res) => {
		const news = await NEWS.getAll(req.query);
		res.status(200).json(news);
	}));

	router.get('/:id', requestValidator(Schemas.getOne), wrapAsync(async (req, res) => {
		const news = await NEWS.getOne(req.params);
		res.status(200).json(news);
	}));

	return router;
};
