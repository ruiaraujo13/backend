'use strict';

const express = require('express');

const router = express.Router(); // eslint-disable-line new-cap

module.exports = () => {
	router.use('/news', require('./news')());

	return router;
};
