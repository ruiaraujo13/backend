'use strict';

require('dotenv').config();

const express = require('express');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const hpp = require('hpp');

const ErrorMessages = require('./errors/error-messages.json');

const app = express();

app.use(helmet());
app.use(compression());
app.use(express.urlencoded({
	extended: false,
	limit: '1mb'
}));
app.use(express.json({
	limit: '5mb'
}));
app.use(hpp());

app.use(cors());
app.use('/v1', require('./transports/webapi/v1')());
app.use('/', require('./transports/webapi/v1')());

app.use((req, res, next) => {
	next(new Error('not found'));
});

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
	console.error('------------------------------------');
	console.error(err);
	console.error('------------------------------------');
	const {
		message,
		status
	} = ErrorMessages[err.message] || ErrorMessages['internal server error'];

	if (res.headersSent === false) {
		res.status(status).json({
			message
		});
	}
});

module.exports = app;
