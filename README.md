## Backend

Node/Express Backend API

#### Data
To facilitate the use of this project, it's used a JSON file mock database. In order to generate new data, use the following command:
```
node mockdata.js
```

##### Run Locally
```
npm start
```

##### Run in Docker Container
```
docker build -t backend .
docker run -d -p 3003:80 -e PORT=80 -e NODE_ENV=production backend
```
