'use strict';

const {
	News: NewsDB
} = require('../db');

module.exports = class News {
	constructor() {
		this._NEWSDB = new NewsDB();
	}

	getAll({
		orderBy,
		orderType,
		filter,
		page,
		perPage
	}) {
		return this._NEWSDB.findAll({
			orderBy,
			orderType,
			filter,
			page,
			perPage
		});
	}

	getOne({
		id
	}) {
		return this._NEWSDB.findOneById({
			id
		});
	}
};
